// TTD - Unit testing for the converter module

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe ("RGB to Hex conversions", () => {
        it("Converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#ff0000"); // red Hex value
            
            const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff
            expect(blueHex).to.equal("#0000ff"); // blue Hex value
            
            const greenHex = converter.rgbToHex(0, 255, 0); // #00ff00
            expect(greenHex).to.equal("#00ff00"); // green Hex value
        })
    })
})